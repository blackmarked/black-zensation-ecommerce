

var headerFunctions = (function ($) {

  // Shows the nav on mobile
  function navToggle() {
    $('#nav--container').toggleClass('active');
  }


  // Shows the search on mobile
  function searchToggle() {

    console.log($(document).width());

    if ( $(document).width() >= 1024 ) {
      return;
    }

    $('#header__search, #mask--search').toggleClass('active');
  }


  // Function that converts header to fixed header on scroll
  function showNavScroll() {

    $(window).scroll(function() {
      if ($(this).scrollTop() > 200) {
        $('#header').addClass('lap-scroll');
      } else {
        $('#header').removeClass('lap-scroll');
      }
    });


  }



  return {


    onLoad: function() {
      showNavScroll();
    },

    onClick: function() {

      $('#nav--trigger').click(function() {
        navToggle();
      });


      $('#search--trigger, #mask--search--close').click(function() {
        searchToggle();
      });

    }


  };

})(jQuery);
