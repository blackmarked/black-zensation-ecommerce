
<div class="row">

    <div class="column--12 column--lap--6 pd--sec">
        <h4><?php echo $text_new_customer; ?></h4>

        <div class="radio">
            <label>
                <?php if ($account == 'register') { ?>
                <input type="radio" name="account" value="register" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="account" value="register" />
                <?php } ?>
                <?php echo $text_register; ?></label>
        </div>
        <?php if ($checkout_guest) { ?>
        <div class="radio">
            <label>
                <?php if ($account == 'guest') { ?>
                <input type="radio" name="account" value="guest" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="account" value="guest" />
                <?php } ?>
                <?php echo $text_guest; ?></label>
        </div>
        <?php } ?>
        <p><?php echo $text_register_account; ?></p>
        <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />

    </div><div class="column--12 column--lap--6 pd--sec divider--l">
        <h4><?php echo $text_returning_customer; ?></h4>

        <div class="form-group">
            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
            <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="input input--100" />
        </div>
        <div class="form-group">
            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
            <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="input input--100 mgb" />
            <a href="<?php echo $forgotten; ?>" class="pd"><?php echo $text_forgotten; ?></a></div>
        <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />

    </div>

</div>
