<?php echo $header; ?>


<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>

    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>


    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>


        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

            <h1>
                My basket
                <!--
                <?php echo $heading_title; ?>
                <?php if ($weight) { ?>
                    &nbsp;(<?php echo $weight; ?>)
                <?php } ?>
                -->
            </h1>

            <div class="row">

                <section class="column--12 column--lap--8">

                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="table-responsive">
                            <table class="table table--basket">
                                <thead>
                                    <tr>
                                        <td class="text-left">Product Details</td>
                                        <td class="text-left"><?php echo $column_quantity; ?></td>
                                        <td class="text-right"><?php echo $column_total; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($products as $product) { ?>
                                    <tr>
                                        <td class="text-left">
                                            <a href="<?php echo $product['href']; ?>">
                                                <div class="basket-details">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img pd" />
                                                </div>
                                                <div class="basket-details">
                                                    <?php echo $product['name']; ?>
                                                </div>
                                            </a>
                                            <?php if (!$product['stock']) { ?>
                                            <span class="text-danger">***</span>
                                            <?php } ?>
                                            <?php if ($product['option']) { ?>
                                            <?php foreach ($product['option'] as $option) { ?>
                                            <br />
                                            <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                            <?php } ?>
                                            <?php } ?>
                                            <?php if ($product['reward']) { ?>
                                            <br />
                                            <small><?php echo $product['reward']; ?></small>
                                            <?php } ?>
                                            <?php if ($product['recurring']) { ?>
                                            <br />
                                            <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                                            <?php } ?></td>
                                        <td class="text-left">

                                            <div class="input-group btn-block" style="max-width: 200px;">
                                                <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="input" />


                                                <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="button--trigger"><i class="fa fa-refresh"></i></button>

                                                <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="button--trigger" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times-circle"></i></button>

                                            </div>

                                        </td>
                                        <td class="text-right"><?php echo $product['total']; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php foreach ($vouchers as $voucher) { ?>
                                    <tr>
                                        <td></td>
                                        <td class="text-left"><?php echo $voucher['description']; ?></td>
                                        <td class="text-left"></td>
                                        <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                                                <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                                                <span class="input-group-btn">
                                                <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $voucher['key']; ?>');"><i class="fa fa-times-circle"></i></button>
                                                </span></div></td>
                                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>


                </section><section class="column--4">

                    <div class="basket--total pd">

                        <table class="table">
                            <?php foreach ($totals as $total) { ?>
                            <tr>
                                <td class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
                                <td class="text-right"><?php echo $total['text']; ?></td>
                            </tr>
                            <?php } ?>
                        </table>

                        <div class="matrix mgb--sec">
                            <a href="<?php echo $checkout; ?>" class="button button--c--red button--lg">
                                <!-- <?php echo $button_checkout; ?> -->
                                <i class="fa fa-lock"></i>
                                <span>Continue to checkout</span>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                        <div class="discount">
                            <?php foreach ($modules as $module) { ?>
                                <?php echo $module; ?>
                            <?php } ?>

                        </div>


                    </div>


                    <!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/au/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/au/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/PP_AcceptanceMarkTray-NoDiscover_243x40.png" alt="Buy now with PayPal" /></a></td></tr></table><!-- PayPal Logo -->


                </section>


            </div>




            <!-- <?php if ($modules) { ?> -->
            <!-- <h2><?php echo $text_next; ?></h2> -->
            <!-- <p><?php echo $text_next_choice; ?></p> -->
            <!-- <div class="panel-group" id="accordion"> -->
            <!-- </div> -->
            <!-- <?php } ?> -->
            <!-- <br /> -->
            <!-- <div class="row"> -->
                <!-- <div class="col-sm-4 col-sm-offset-8"> -->
                <!-- </div> -->
            <!-- </div> -->
            <!-- <div class="buttons clearfix"> -->
                <!-- <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a></div> -->
                <!-- <div class="pull-right"></div> -->
            <!-- </div> -->


            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>


<?php echo $footer; ?>
