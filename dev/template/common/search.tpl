
    <input id="header__search" type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>..." class="input" />
    <i id="search--trigger" class="header__search__icon fa fa-search"></i>
    <button type="button" class="btn btn-default btn-lg" style="display: none;"></button>
    <div id="mask--search" class="mask mask--visible">
        <i id="mask--search--close" class="fa fa-close"></i>
    </div>
    <!--
    <span class="input-group-btn">
    </span>
    -->
