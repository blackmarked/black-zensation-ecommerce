<!DOCTYPE html>

<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->


    <head>

        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>


        <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <!-- <link href="catalog/view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" /> -->
        <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/jquery/instafeed.min.js" type="text/javascript"></script>
        <script src="catalog/view/theme/zensation/js/zensation.js" type="text/javascript"></script>



        <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600" rel="stylesheet">
        <!-- <link href="catalog/view/theme/zensation/stylesheet/stylesheet.css" rel="stylesheet"> -->
        <link href="catalog/view/theme/zensation/css/zensation.css" rel="stylesheet">



        <meta name="twitter:title" content="<?php echo $title; ?>">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@ZensationSydney">
        <?php if ($description) { ?>
        <meta name="twitter:description" content="<?php echo $description; ?>">
        <?php } ?>
        <meta name="twitter:image" content="http://zensation.com.au/image/cache/catalog/store/christina-400x267.jpg">


        <meta property="fb:admins" content="694930382">
        <meta property="og:locale" content="en_AU">
        <meta property="og:type" content="Website">

        <meta property="og:title" content="<?php echo $title; ?>">
        <?php if ($description) { ?>
        <meta property="og:description" content="<?php echo $description; ?>">
        <?php } ?>
        <meta property="og:image" content="http://zensation.com.au/image/cache/catalog/store/christina-400x267.jpg">
        <meta property="og:url" content="http://zensation.com.au/">
        <meta property="og:site_name" content="Zensation Tea House">




        <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/modernizr.js" type="text/javascript"></script>
        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>

    </head>


    <body class="<?php echo $class; ?>">

        <!--
        <nav id="top" class="nav--helper">
            <div class="nav--helper--container cf">
                <?php /* echo $currency; */ ?>
                <?php /* echo $language; */ ?>
                <div id="top-links" class="nav pull-right">
                    <ul class="list-inline">
                        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></a></li>
                        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php if ($logged) { ?>
                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                <?php } else { ?>
                                <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                                <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
                        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
                        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
                    </ul>
                </div>
            </div>
        </nav>
        -->

        <header id="header" class="header">

            <div class="header__container">


                <div class="column--5 column--palm--4 header__search vam">
                    <i id="nav--trigger" class="header__trigger--nav fa fa-bars"></i>
                    <?php echo $search; ?>

                </div><div class="column--2 column--palm--4 vam tac">
                    <div class="header__logo">
                        <a href="<?php echo $home; ?>">
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img" />
                        </a>
                    </div>

                </div><div class="column--5 column--palm--4 header__user vam tar">
                    <a href="<?php echo $account; ?>" class="fa fa-user"></a>
                    <?php echo $cart; ?>
                </div>


            </div>

            <div id="nav--container" class="header__container--nav">

                    <?php if ($categories) { ?>
                        <nav class="nav--header">
                            <!--
                            <div class="navbar-header">
                                <button type="button" class="button button--trigger nav--header__btn" data-toggle="collapse" data-target=".navbar-ex1-collapse" onclick="$('header').toggleClass('nav-open'); $('html').toggleClass('overflow');"><i class="fa fa-bars"></i></button>
                            </div>
                            -->
                            <div class="nav--header__container">
                                <ul class="nav navbar-nav">
                                    <li><a href="<?php echo $home; ?>">Home</a></li>
                                    <?php foreach ($categories as $category) { ?>
                                    <?php if ($category['children']) { ?>
                                    <li class="dropdown">
                                        <a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo $category['name']; ?>
                                        </a>
                                        <div class="dropdown-menu">
                                            <div class="dropdown-inner">
                                                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                <ul class="list-unstyled">
                                                    <?php foreach ($children as $child) { ?>
                                                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                    <?php } ?>
                                                    <li><a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></li>
                                                </ul>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </li>
                                    <?php } else { ?>
                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>

                                    <li><a href="/index.php?route=information/information&information_id=4">About</a></li>
                                    <li><a href="<?php echo $contact; ?>">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    <?php } ?>

                </div>

                <!-- </div> -->
            </div>


        </header>

        <div class="banner banner--header">
            <i class="fa fa-truck"></i>
            <span>Free Delivery on Orders Over $60</span>
        </div>
