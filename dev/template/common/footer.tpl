        <footer class="footer">


            <div class="matrix footer--container">

                <section class="column--12 column--lap--10">

                    <?php if ($informations) { ?>
                    <div class="column--12 column--lap--3">
                        <h6><?php echo $text_information; ?></h6>
                        <ul class="list-unstyled mgb--ter">
                            <?php foreach ($informations as $information) { ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } ?>
                        </ul>

                    </div><?php } ?><div class="column--12 column--lap--3">
                        <h6><?php echo $text_service; ?></h6>
                        <ul class="list-unstyled mgb--ter">
                            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                            <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                            <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                        </ul>

                    </div><div class="column--12 column--lap--3">
                        <h6><?php echo $text_extra; ?></h6>
                        <ul class="list-unstyled mgb--ter">
                            <!-- <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li> -->
                            <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                            <!-- <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li> -->
                            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                        </ul>

                    </div><div class="column--12 column--lap--3">
                        <h6><?php echo $text_account; ?></h6>
                        <ul class="list-unstyled mgb--ter">
                            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                            <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                        </ul>
                    </div>

                </section><section class="column--12 column--lap--2">
                    <h6>Pay Securely</h6>
                    <div class="footer__payments">
                        <!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/au/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/au/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/PP_AcceptanceMarkTray-NoDiscover_243x40.png" alt="Buy now with PayPal" /></a></td></tr></table><!-- PayPal Logo -->
                    </div>
                </section>


                <div class="row">
                <div class="row footer__copy">
                    <p><?php echo $powered; ?></p>
                </div>
            </div>
        </footer>

        <script>
            $(document).ready(function() {
                headerFunctions.onLoad();
                headerFunctions.onClick();
            });
        </script>


    </body>

</html>
