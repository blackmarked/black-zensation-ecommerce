
<?php echo $header; ?>

        <section class="maintenance">

            <div class="container">
                <div class="row row--dense uppercase text-shadow tac">
                    <h1>Zensation</h1>
                    <p class="gamma light">Tea House</p>
                </div>
                <div class="row row--dense gamma text-shadow tac">
                    <p>We are currently performing some scheduled maintenance.</p>
                    <p>
                        We will be back online asap.<br>
                        Please check back later.
                    </p>
                </div>
            </div>

        </section>

        <footer class="footer footer--maintenance">
            <div class="matrix footer--container">

                <div class="row footer--copy">
                    <p>
                        Built by <a href="http://black-marked.com">Black Marked</a><br>
                        Zensation Tea House &copy; 2016
                    </p>
                </div>

            </div>
        </footer>


    </body>

</html>

