<?php echo $header; ?>


<div class="container">

    <div class="matrix">

        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <div id="content">
            <!-- Class for sliders -->
            <!-- class="<?php echo $class; ?>"> -->
            <?php echo $content_top; ?>
            <?php echo $content_bottom; ?>
        </div>


        <?php echo $column_right; ?>

    </div>

</div>

<?php echo $footer; ?>

<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: '24704084',
        limit: '18',
        accessToken: '24704084.1677ed0.3aeeb5715af04baa8b75aa95d3d94bdd',
        resolution: 'low_resolution',
        template: '<div class="column--6 column--palm--4 column--lap--2"><a href="{{link}}" target="_blank"><img src="{{image}}" class="img" /></a></div>'
        // clientId: '3eaae331bdf24f289779603fd3d6db57'
    });
    feed.run();
</script>
