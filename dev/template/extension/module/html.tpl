
<?php if($heading_title) { ?>
    <div class="feature--<?php echo $heading_class; ?>">
<?php } else { ?>
    <div>
<?php } ?>

    <div class="row row--mid">
        <?php if($heading_title) { ?>
            <h3><?php echo $heading_title; ?></h3>
        <?php } ?>
        <?php echo $html; ?>
    </div>

</div>
