<div class="row row--mid mgb--ter featured-container">
    <h3>Tea you need to try</h3>
    <!-- <h3><?php echo $heading_title; ?></h3> -->

    <?php foreach ($products as $product) { ?>

    <div class="product col-lg-3 col-md-3 col-sm-6 col-xs-12">

        <div class="product--container transition">

            <div class="product--image">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
            </div>

            <a href="<?php echo $product['href']; ?>" class="product--caption">
                <div class="product--title">
                    <h6><?php echo $product['name']; ?></h6>
                    <button type="button" class="button--trigger button--trigger--wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>'); return false;"><i class="fa fa-heart"></i></button>
                </div>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['price']) { ?>
                <p class="product--price tac">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span><br>
                        <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                    <!-- <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span> -->
                    <?php } ?>
                </p>
                <?php } ?>

                <?php if ($product['rating']) { ?>
                <div class="product--rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>


            </a>
            <!-- <div class="product--container--button"> -->
                <!-- <button type="button" class="button button--outline button--outline--accent" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button> -->
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                -->
            <!-- </div> -->


        </div>
    </div>

    <?php } ?>


</div>
