
<div class="matrix mgb--sec">


    <!-- <div class="panel-heading"> -->
        <h6 class="mgb--rst">
            Have a gift certificate?
            <!-- <a href="#collapse-voucher" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $heading_title; ?> <i class="fa fa-caret-down"></i></a> -->
        </h6>
    <!-- </div> -->


    <!-- <div id="collapse-voucher" class="panel-collapse collapse"> -->
        <!-- <div class="panel-body"> -->

            <!-- <label class="col-sm-2 control-label" for="input-voucher"><?php echo $entry_voucher; ?></label> -->


            <div class="column--8 vam pd--rst">
                <input type="text" name="voucher" value="<?php echo $voucher; ?>" placeholder="Enter your code here" id="input-voucher" class="input input--bg--white input--100" />

            </div><div class="column--4 vam pd--rst">
                <input type="submit" value="Submit" id="button-voucher" data-loading-text="<?php echo $text_loading; ?>" class="button button--lg button--c--accent" />

            </div>




            <script type="text/javascript"><!--
                $('#button-voucher').on('click', function() {
                    $.ajax({
                        url: 'index.php?route=extension/total/voucher/voucher',
                        type: 'post',
                        data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
                        dataType: 'json',
                        beforeSend: function() {
                            $('#button-voucher').button('loading');
                        },
                        complete: function() {
                            $('#button-voucher').button('reset');
                        },
                        success: function(json) {
                            $('.alert').remove();

                            if (json['error']) {
                                $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                            }

                            if (json['redirect']) {
                                location = json['redirect'];
                            }
                        }
                    });
                });
            //--></script>


        <!-- </div> -->
    <!-- </div> -->
</div>
