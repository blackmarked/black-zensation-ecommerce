
var gulp            = require('gulp'),
    sass            = require('gulp-ruby-sass'),
    sequence        = require('run-sequence'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins         = gulpLoadPlugins({
                        pattern: ['gulp-*', 'gulp.*'],
                        replaceString: /^gulp(-|\.)/,
                        rename: {
                          'gulp-concat-sourcemap'    : 'concatSourcemap',
                          'gulp-merge-media-queries' : 'mmq',
                          'gulp-minify-inline'       : 'minifyInline',
                          'gulp-clean-css'           : 'cleanCSS',
                          'gulp-scss-lint'           : 'scssLint',
                          'gulp-scss-lint-stylish'   : 'scssLintStylish',
                          'gulp-cache-bust'          : 'cachebust'
                        }
                      });





var pkg = require('./package.json');
// Print this to the scss and js on build
var banner = [
  '',
  ' ',
  '/**',
  ' * Version: <%= pkg.version %>',
  ' * Author',
  ' * <%= pkg.author %> [black-marked.com]',
  ' * Copyright (c) 2015 black-marked',
  ' * All rights reserved',
  '**/',
  ' ',
  ''
].join('\n');





// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// GLOBS
var paths = {

  dev_base      : 'dev/',
  dev_template  : 'dev/template/',
  dev_scss      : 'dev/scss/',
  dev_js        : 'dev/js/',

  prod_base     : './www/catalog/view/theme/zensation/',
  prod_template : './www/catalog/view/theme/zensation/template/',
  prod_css      : './www/catalog/view/theme/zensation/css/',
  prod_js       : './www/catalog/view/theme/zensation/js/'

};
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
var isBuild      = false,
    mmq_log      = true,
    scssSM       = true,
    dropConsole  = false,
    action       = 'dev',
    releaseCheck = [],
    tasks        = [
                    'template',
                    'scss',
                    'js'
                  ];



if ( plugins.util.env.build === true ) {
  // --build flag

  isBuild      = true;
  dropConsole  = true;
  mmq_log      = false;
  scssSM       = false;
  action       = 'build';

}
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //





// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.  8888888888 88888888888 888     888 8888888b.
// d88P  Y88b 888            888     888     888 888   Y88b
// Y88b.      888            888     888     888 888    888
//  "Y888b.   8888888        888     888     888 888   d88P
//     "Y88b. 888            888     888     888 8888888P"
//       "888 888            888     888     888 888
// Y88b  d88P 888            888     Y88b. .d88P 888
//  "Y8888P"  8888888888     888      "Y88888P"  888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('setup', function (cb) {

  return gulp.src('*.js', {read: false})
    .pipe(plugins.shell( 'bundler install'))
    .on( 'end', function() { plugins.util.log( plugins.util.colors.blue( '\n' + '\n' + 'Environment setup' + '\n') ); }, cb);
});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //











// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//   888888  .d8888b.
//     "88b d88P  Y88b
//      888 Y88b.
//      888  "Y888b.
//      888     "Y88b.
//      888       "888
//      88P Y88b  d88P
//      888  "Y8888P"
//    .d88P
//  .d88P"
// 888P"
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_js = {

  js: [
    paths.dev_js + '*.js',
    paths.dev_js + '**/*.js'
  ]

};




gulp.task('js', function (cb) {

  console.log(
    ' ' + '\n' +
    'Javascript Started'
  );

  return gulp.src(paths_js.js)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: JS Error')}))
    .pipe(isBuild ? plugins.util.noop() : plugins.cached('js-cache'))
    .pipe(isBuild ? plugins.util.noop() : plugins.remember('js-cache'))
    .pipe(isBuild ? plugins.util.noop() : plugins.print())
    .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.init())
    .pipe(isBuild ? plugins.concat('zensation.js') : plugins.concatSourcemap('zensation.js'))
    .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.write())
    .pipe(plugins.uglify({
      compress: {
        drop_console: dropConsole
      }
    }))
    .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : plugins.util.noop())
    .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : plugins.util.noop())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_js))
    .pipe(plugins.livereload(), cb);
});



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //









// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 88888888888 8888888888 888b     d888 8888888b.  888             d8888 88888888888 8888888888
//     888     888        8888b   d8888 888   Y88b 888            d88888     888     888
//     888     888        88888b.d88888 888    888 888           d88P888     888     888
//     888     8888888    888Y88888P888 888   d88P 888          d88P 888     888     8888888
//     888     888        888 Y888P 888 8888888P"  888         d88P  888     888     888
//     888     888        888  Y8P  888 888        888        d88P   888     888     888
//     888     888        888   "   888 888        888       d8888888888     888     888
//     888     8888888888 888       888 888        88888888 d88P     888     888     8888888888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



var paths_template = {

  template: [
    paths.dev_template + '*.tpl',
    paths.dev_template + '**/*.tpl'
  ]

};

gulp.task('template', function (cb) {

  console.log(
    ' ' + '\n' +
    'Template Started'
  );

  return gulp.src(paths_template.template)
    .pipe(isBuild ? plugins.util.noop() : plugins.print())
    .pipe(isBuild ? plugins.util.noop() : plugins.cached('template-cache'))
    .pipe(plugins.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      includeAutoGeneratedTags: false
    }))
    // .pipe(plugins.minifyInline())
    // .pipe(isBuild ? plugins.cachebust() :  plugins.util.noop())
    .pipe(gulp.dest(paths.prod_template))
    .pipe(plugins.livereload(), cb);

});
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //












// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
//  .d8888b.   .d8888b.   .d8888b.   .d8888b.
// d88P  Y88b d88P  Y88b d88P  Y88b d88P  Y88b
// Y88b.      888    888 Y88b.      Y88b.
//  "Y888b.   888         "Y888b.    "Y888b.
//     "Y88b. 888            "Y88b.     "Y88b.
//       "888 888    888       "888       "888
// Y88b  d88P Y88b  d88P Y88b  d88P Y88b  d88P
//  "Y8888P"   "Y8888P"   "Y8888P"   "Y8888P"
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_scss = {

  scss_master: paths.dev_scss + 'zensation.scss',

  scss_watch: [
    paths.dev_scss + '*.scss',
    paths.dev_scss + '**/*.scss'
  ],

  css_master: paths.prod_css + 'zensation.css'

};



gulp.task('scss', ['lint_scss'], function (cb) {

  console.log(
    ' ' + '\n' +
    'SCSS Started'
  );


  return sass(paths_scss.scss_master, {
    // bundleExec: true,
    sourcemap: scssSM
  })
  .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: SCSS Lint Error')}))
  .on('error', function (err) {
    console.error('Error!', err.message);
  })
  .pipe(isBuild ? plugins.util.noop() : plugins.sourcemaps.write())
  .pipe(plugins.mmq({log: mmq_log}))
  .pipe(plugins.plumber.stop())
  .pipe(gulp.dest(paths.prod_css))
  .pipe(isBuild ? plugins.cleanCSS() : plugins.util.noop())
  .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : plugins.util.noop())
  .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : plugins.util.noop())
  .pipe(gulp.dest(paths.prod_css))
  .pipe(plugins.livereload(), cb);

});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //



var paths_reloader = {

  everything_watch: [
    paths.prod_base + '*.*',
    paths.prod_base + '**/*.*',
    '!'+paths.prod_base + 'zensation.css',
    '!'+paths.prod_base + '**/zensation.css'
  ]

};


gulp.task('livereloader', function (cb) {

  console.log(
    ' ' + '\n' +
    'Reload'
  );

  return gulp.src('*.js', {read: false})
  .pipe(plugins.livereload(), cb);

});






// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 888      8888888 888b    888 88888888888
// 888        888   8888b   888     888
// 888        888   88888b  888     888
// 888        888   888Y88b 888     888
// 888        888   888 Y88b888     888
// 888        888   888  Y88888     888
// 888        888   888   Y8888     888
// 88888888 8888888 888    Y888     888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


var paths_lint = {

  scss: [
    paths.dev_scss + '*.scss',
    paths.dev_scss + '**/*.scss',
    '!'+paths.dev_scss + 'tools/**',
    '!'+paths.dev_scss + 'libs/**',
    '!'+paths.dev_scss + 'objects/_icons.scss',
    '!'+paths.dev_scss + 'generic/_reset.scss',
    '!'+paths.dev_scss + 'generic/_shared.scss',
    '!'+paths.dev_scss + 'generic/_type.scss'
  ]

};


gulp.task('lint_scss', function (cb) {

  console.log(
    ' ' + '\n' +
    'SCSS lint'
  );

  return gulp.src(paths_lint.scss)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError("Error: SCSS lint error")}))
    .pipe(isBuild ? plugins.util.noop() : plugins.cached('scss-lint-cache'))
    .pipe(plugins.scssLint({
      'config': 'lint.yml',
      'customReport': plugins.scssLintStylish,
      'bundleExec': true,
      'maxBuffer': 300 * 1024
    }))
    .pipe(plugins.scssLint.failReporter(), cb);
});



// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //




// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// 888       888        d8888 88888888888 .d8888b.  888    888
// 888   o   888       d88888     888    d88P  Y88b 888    888
// 888  d8b  888      d88P888     888    888    888 888    888
// 888 d888b 888     d88P 888     888    888        8888888888
// 888d88888b888    d88P  888     888    888        888    888
// 88888P Y88888   d88P   888     888    888    888 888    888
// 8888P   Y8888  d8888888888     888    Y88b  d88P 888    888
// 888P     Y888 d88P     888     888     "Y8888P"  888    888
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //


gulp.task('watch', function () {


  plugins.livereload.listen();


  console.log(
    ' ' + '\n' +
    '     [ black-marked ]' + '\n' +
    ' '
  );


  var watcherJS = gulp.watch(paths_js.js, [
    'js'
  ]);
  watcherJS.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['js-cache'];
      plugins.remember.forgetAll('js-cache');
    }
  });

  var watcher_template = gulp.watch(paths_template.template, [
    'template'
  ]);
  watcher_template.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['template-cache'];
    }
  });


  gulp.watch(paths_scss.scss_watch, ['scss']);
  gulp.watch(paths_reloader.everything_watch, ['livereloader']);


});

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //






gulp.task('dev', function (cb) {
  sequence('setup', tasks, 'watch', cb);
});

gulp.task('build', function (cb) {
  sequence('setup', tasks, 'watch', cb);
});

gulp.task('default', [ action ]);


