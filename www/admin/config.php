<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8888/admin/');
define('HTTP_CATALOG', 'http://localhost:8888/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8888/admin/');
define('HTTPS_CATALOG', 'http://localhost:8888/');

// DIR
define('DIR_APPLICATION', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/admin/');
define('DIR_SYSTEM', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/');
define('DIR_IMAGE', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/image/');
define('DIR_LANGUAGE', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/admin/language/');
define('DIR_TEMPLATE', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/admin/view/template/');
define('DIR_CONFIG', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/config/');
define('DIR_CACHE', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/storage/cache/');
define('DIR_DOWNLOAD', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/storage/download/');
define('DIR_LOGS', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/storage/logs/');
define('DIR_MODIFICATION', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/storage/modification/');
define('DIR_UPLOAD', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/system/storage/upload/');
define('DIR_CATALOG', '/Applications/AMPPS/www/black-marked-projects/zensation/black-zensation-ecommerce/www/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'zensatio_ecoAdmi');
define('DB_PASSWORD', 'Zseadbpw02');
define('DB_DATABASE', 'zensatio_eco');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
